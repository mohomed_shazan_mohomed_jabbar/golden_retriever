\documentclass[conference]{IEEEtran}
\usepackage{graphicx}
\usepackage{url}

\begin{document}
\title{Golden Retriever: Question Retrieval System}

\author{
	\IEEEauthorblockN{
		Hamman W. Samuel,
		Mi-Young Kim,
		Sankalp Prabhakar,
		Mohomed Shazan Mohomed Jabbar}
	\IEEEauthorblockA{
		Department of Computing Science, University of Alberta,
		Edmonton, Canada\\ 
		Email: \{hwsamuel,miyoung2,sankalp,mohomedj\}@ualberta.ca}}

\maketitle

\begin{abstract}
Duplicate questions get posted on Q\&A online forums because users may not be aware of similar questions. Our proposed system, Golden Retriever, can recommend existing questions that are semantically related to incoming questions. Compared with other existing techniques such as Latent Semantic Indexing, Language Model and Semantic Similarity, our approach shows good results for the ICHI Healthcare Data Analytics Challenge dataset using normalized TF-IDF, relevance heuristics, and semantic relatedness.
\end{abstract}

\IEEEpeerreviewmaketitle

\section{Introduction}
In recent years Question and Answer (Q\&A) forums which provide valuable healthcare information have become quite popular. Large numbers of questions and answers archived in these forums provide a valuable knowledge base to patients and caregivers. One major challenge in such knowledge base is that users may ask the same question repetitively, either because of not being able to spend time to find similar questions on their own, or due to lack of domain knowledge, leading to a significant increase in repetitive questions. We develop a system called ``Golden Retriever'' to address this challenge and to retrieve and recommend semantically related questions.

\section{Related Work}
Retrieval of similar questions in community-based question answering has been studied extensively in 
\cite{Luo15, Ji12, Xue08}. Luo et al. \cite{Luo15} compare the similarity of archived health questions and retrieve answers using syntactic and semantic analysis. They employ Dice coefficient and cosine similarity by adding syntactic and semantic features. However, the characteristics of their questions are different from ours: each question in their experiments consists of only one short sentence, and their questions do not have a lot of noisy terms. Because a question in our data consists of many sentences, we cannot extract only one sentence as the question, and syntactic analysis for each sentence is not helpful in understanding the whole meaning.  

Ji et al. \cite{Ji12} propose a question-answer topic model and use corresponding answers for topic model learning. In this approach, they have to assume that a question and its paired answer share the same topic distribution. In our challenge, the dataset does not include corresponding answers, and we have also observed that a question-answer pair does not share the same prior distribution over topics in a related patient forum. 

Another approach uses language model. Xue et al. \cite{Xue08} propose translation-based language model which learns word-to-word translation probabilities by considering Q\&A archive as a type of parallel corpus. This approach works well in general domain where questions can cover a variety of topics. However, in our challenge, the topic is restricted to Type 2 diabetes and most of the words in questions and answers are overlapped. 

\section{Methodology}
Our approach approximates users' information seeking behaviour~\cite{Ellis1997}, which includes domain experts and non-experts. Given an incoming question, the expert searches for similar questions in the corpus by determining relevant keywords from the incoming question. Corpus questions that contain these keywords are candidates for matches. The expert also has a sense of semantically related words, such as synonyms and abbreviations, that could be matched in the corpus. Finally, the user has knowledge of the context of the incoming question and what exactly is being asked. 

\subsection{Pre-processing}
As part of pre-processing, the questions corpus is indexed. Regular expression tokenization is applied to each question and stop words are filtered using the modified Glasgow Stop Words List from the TAPoRware project~\cite{TAPoRware}. The incoming question is also pre-processed by tokenization and stop words filtering. In addition, we expand any keywords that are medical abbreviations to their full forms. To get abbreviations, we use a subset of the list from~\cite{MedAbbr}. The incoming question's tokenized keywords are then matched with the corpus index and scored.

\subsection{Scoring}
For keyword matching, we compute the TF-IDF for each keyword in the incoming question using the corpus index. The Term Frequency-Inverse Document Frequency (TF-IDF) measure is well-known in literature as a weighting factor for word relevance in documents~\cite{Ramos2003}. Given an incoming question, a matching score is determined for each corpus question, $score(C_j) = \sum \displaystyle \frac{\texttt{tfidf}(k_i, C_j)}{max_{tf}}$, where $max_{tf} = \texttt{max}(\texttt{tf}(w,C_j))$, and \texttt{tf} is the term frequency of a keyword $w$ in the corpus question $C_j$, while $k_i$ represents the keywords in the incoming question. We use the same formula for \texttt{tfidf}$(k_i,C_j)$ as in~\cite{Ramos2003}. The best matches in the corpus are then determined by sorting the scores. The TF-IDF score is normalized by ${max_{tf}}$. The score is also adjusted using relevance heuristics and semantic relatedness.

\subsection{Relevance heuristics}
We note that the title of the question is a summarization of the question. Consequently, the keywords extracted from the incoming question title are given a higher score by using a constant relevance weight. Also, keywords matched with the corpus question title are given an additional higher score. For determining relevance within the question's body keywords, we observe that various words are not relevant even after stop words are filtered. We apply another filter by removing all non-medical terms. The medical terms are determined by using Merriam-Webster's Medical Dictionary API~\cite{Merriam}.  We use the Lancaster Stemming algorithm~\cite{Lancaster} to compare keywords to medical words and take into consideration different word forms, such as singularization/pluralization.

\subsection{Semantic relatedness}
Some words are semantically related but not similar, such as \textit{libido} and \textit{impotence}, or \textit{sugar} and \textit{glucose}. The domain expert has an understanding of these words as related, but most dictionaries, ontologies and thesauri we investigated do not group such words together. We ultimately used Moby Thesaurus~\cite{Ward1996} for synonym lookups because both semantically similar and semantically related words are grouped together. The Moby Thesaurus contains over 30,000 root words, 2.5 million synonyms and related words. Synonyms are used only in situations where no matches are found for an incoming question's keyword within the corpus. Moby Thesaurus is used to look up synonyms, and the TF-IDF for the synonyms is used as a score for the original keyword. The number of synonyms that match corpus questions is used as an offsetting weight. This is necessary to balance the possible inflation of scores for a keyword having many synonyms. As an additional relevance heuristic, synonyms are also filtered by removing all non-medical keywords that are not in the corpus titles.

\section{Results}
We base our results on self-curated annotations for each incoming question. In other words, we manually select the best possible matches (up to 3) from the corpus question set and use it as a benchmark to measure our results. We obtain the best performance with the Golden Retriever system, referred to here as the Current Model (CM). Using the CM, we get a recall of 71.8\% for the ICHI Healthcare Data Analytics Challenge dataset. The recall is calculated for each incoming question based on the number of matches it has in the created annotations, which is then averaged out for all questions in the incoming questions set. The execution time is around 35 seconds on a machine with CPU 3.40GHz and 8GB system memory.

We also tried other modelling techniques like Latent Semantic Indexing (LSI)~\cite{Deerwester1990}, Language Model-based Information Retrieval (LM)~\cite{Ponte1998}, and Semantic Similarity (SS)~\cite{Leacock1998}. These are some of the most popular models for pattern matching between terms and concepts contained in an unstructured collection of text. In some cases these models worked well in detecting accurate patterns, but the overall results did not fare well compared to our Current Model (CM). The LSI model was trained on the given set of corpus questions for which a recall of 40\% was observed. Compared to LSI, we got better results using Semantic Similarity (SS) and Language Model-based information retrieval (LM). The recall values from these models were 65\% and 50\% respectively. A recall rate comparison graph for all these models is shown in Fig. 1.

\begin{figure}[ht!]
	\centering	
	\includegraphics[scale=0.45]{recall_final.png}
	\centering{Fig. 1. Comparison Graph: Recall Curves}
\end{figure}

\section{Conclusion}
A lot of duplicate questions are posted on Q\&A forums because users may not be able to spend time to find similar questions, or they lack domain knowledge. Our proposed system, Golden Retriever, finds existing questions that are similar to new questions. Compared with other popular approaches, our system shows good results using normalized TF-IDF, relevance heuristics, and semantic relatedness.

\bibliographystyle{IEEEtran}  
\bibliography{goldenretriver}  
\end{document}
'''
    Similar questions recommender using tf-idf and semantic relatedness
    Hamman W. Samuel, Mi-Young Kim, Sankalp Prabhakar, Mohomed Shazan Mohomed Jabbar
    Department of Computing Science, University of Alberta, Canada
    July 2015
'''

from operator import itemgetter
from sys import argv
from math import log10
from helper import *
from docindex import DocIndex

def main(incoming_file,corpus_file):
    stop_words = [stem(w) for w in load_file('glasgow')]
    med_words = load_file('med_words')
    med_words = [stem(w.decode('latin1').encode('ascii', 'xmlcharrefreplace')) for w in med_words]
    syndict = load_thesaurus('mobythes.aur')
    abbr = load_abbreviations('abbreviations')
    
    incoming_questions = load_questions(incoming_file)
    corpus_questions = load_questions(corpus_file)
    cindex = DocIndex()
    results = {}

    for cquestion in corpus_questions:
        id = cquestion[0]
        title = cquestion[1]
        body = cquestion[2]
        
        title_tokens = tokenize(title,stop_words)
        body_tokens = tokenize(body,stop_words)
        
        append_abbr(title_tokens,abbr)
        append_abbr(body_tokens,abbr)
        
        cindex.add_document(title_tokens,id)
        cindex.add_document(body_tokens,id)
    
    N = cindex.num_documents()
    for iquestion in incoming_questions:
        iquestionid = iquestion[0]
        ititle_tokens = tokenize(iquestion[1],stop_words)
        ibody_tokens = tokenize(iquestion[2],stop_words)
        ibody_tokens = [w for w in ibody_tokens  if stem(w) in med_words] 
        iunigrams = ititle_tokens
        iunigrams.extend(ibody_tokens)
        append_abbr(iunigrams,abbr)
        iunigrams = deduplicate(iunigrams)
        
        scores = {}
        for iunigram in iunigrams: 
            df = cindex.df(iunigram)
            if df == 0:
                if syndict.has_key(iunigram):
                    syns = deduplicate(flatten([s for s in synonyms(iunigram,syndict) if s is not []]))
                    score_inverse_weight = len([s for s in syns if cindex.df(s) > 0])
                else:
                    continue
            else:
                syns = [iunigram]
                score_inverse_weight = 1
            
            for syn in syns:
                df = cindex.df(syn)
                if df == 0:
                    continue
                else:
                    df += 1
                
                for cquestionid in cindex.documents:
                    if not in_title(syn,cquestionid,corpus_questions,stop_words) and stem(syn) not in med_words:
                        continue
                    tf = cindex.tf(syn,cquestionid)
                    if tf == 0:
                        continue
                
                    score = (tf*log10(N/df))/cindex.max_tf(cquestionid)
                    if in_title(iunigram,iquestionid,incoming_questions,stop_words):
                        score *= 2
                    if in_title(syn,cquestionid,corpus_questions,stop_words):
                        score *= 2
                    score /= score_inverse_weight
                    scores[cquestionid] = scores.get(cquestionid,0) + round(score,3)
        sorted_matches = [int(m[0]) for m in sorted(scores.items(), key=itemgetter(1),reverse=True)]
        print str(iquestionid) + ' [' + ' '.join(str(s) for s in sorted_matches[:3]) + ']'
        results[int(iquestionid)] = sorted_matches[:3]
    return results

if __name__ == '__main__':
    if len(argv) == 3:
        results = main(argv[1],argv[2])
    else:
        print('Usage: python run.py incoming corpus')
        print(' python - Version 2.7+')
        print(' incoming - Incoming questions file')
        print(' corpus - Corpus questions file')
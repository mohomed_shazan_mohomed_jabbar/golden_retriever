from lxml import html
from requests import get
from random import randint
from string import ascii_lowercase
from time import sleep

def scrape_dict():
    base = 'http://www.merriam-webster.com/browse/medical/'
    groups = list(ascii_lowercase)
    
    for group in groups:
        sleep(randint(1,5))
        alphabet_url = base + group + '.htm'
        alphabet_page = get(alphabet_url)
        alphabet_tree = html.fromstring(alphabet_page.text)
        splash_url = alphabet_tree.xpath('//form/a/@href')

        if len(splash_url) > 0:
            alphabet_page = get(splash_url)
            alphabet_tree = html.fromstring(alphabet_page.text)
        
        words_list_url = alphabet_tree.xpath('//div[2]/div/ol/li/a/@href')
        for list_url in words_list_url:
            sleep(randint(1,5))
            words_page = get(base + list_url)
            words_tree = html.fromstring(words_page.text)
            splash_url = words_tree.xpath('//form/a/@href')
            if len(splash_url) > 0:
                words_page = get(splash_url)
                words_tree = html.fromstring(words_page.text)
            
            words = words_tree.xpath('//div[5]/ol/li/a/text()')
            for word in words:
                print word.encode('utf-8')

if __name__ == '__main__':
    scrape_dict()
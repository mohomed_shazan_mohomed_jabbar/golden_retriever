GOLDEN RETRIEVER

1.0 GROUP MEMBERS
-----------------
Hamman W. Samuel (hwsamuel@ualberta.ca)
Mi-Young Kim (miyoung2@ualberta.ca)
Sankalp Prabhakar (sankalp@ualberta.ca)
Mohomed Shazan Mohomed Jabbar (mohomedj@ualberta.ca)

2.0 SOURCE TREE
---------------
The source files included are as follows.

run.py 		- Main program
results.txt	- Results of the system on challenge dataset
docindex.py 	- Document indexer
helper.py 	- Helper functions
med_words	- Cache of medical words taken from Merriam-Webster's Medical Dictionary API
glasgow		- Glasgow modified stop words list taken from the TAPoRware project
mobythes.aur	- Open source Moby Thesaurus
abbreviations 	- List of relevant medical abbreviations and their full form
self_curated_dataset.txt - List of best matches for each incoming test question (created by team)

3.0 RUNNING INSTRUCTIONS
------------------------
This program is written and tested on Python version 2.7.6. To execute the program, you need to have Python 2.7.x installed. The program also depends on the NLTK library. Installation instructions are as follows. Hardware configuration for tests was CPU 3.40GHz and 8GB system memory, execution time approximately 35 seconds.

3.1 Python Installation
-----------------------
Python can be downloaded for free from http://python.org/download/releases/2.7.6/ Operating system-specific instructions can be found on http://wiki.python.org/moin/BeginnersGuide/Download

3.2 Dependencies Installation
-----------------------------
The NLTK Python library is required by this program. Installation instructions can be found on http://nltk.org/install.html

3.3 Dependency Files
--------------------
The Modified Glasgow Stop Words list is used from the TAPoRware Project. The words list is provided in the zipped program folder along with the program. It needs to be in the same directory as the program. Other dependency code files are also included in the source code. For specifying incoming questions and questions corpus, please see the CLI below.

The program uses the open source Moby Thesaurus available for download from Project Moby at http://icon.shef.ac.uk/Moby/mthes.html The thesaurus is included as part of the source code.

The program uses cached word entries from Merriam-Webster's Medical Dictionary (excluding all definitions and word relations), included in the source code. The Merriam-Webster's Medical Dictionary API is available for free for non-commercial use at http://www.dictionaryapi.com

The program also uses a subset of medical abbreviations and their expanded form. The original list is downloaded from https://en.wikipedia.org/wiki/List_of_medical_abbreviations and the filtered list is available with the source code. 

3.4 Command Line Interface (CLI)
--------------------------------
The program is run from the command line via the following command.

python run.py incoming corpus

where
	python - Version 2.7+
        incoming - Incoming questions file, e.g. ICHI-Challenge-Sample-Test-Questions.txt
        corpus - Corpus questions file, e.g. ICHI-Challenge-Sample-Question-Corpus.txt

3.5 Results & Output
--------------------
The results are printed to the command prompt but can also be saved via stdout if needed. For example python run.py incoming corpus > results.txt

The results are formatted as

IQ1 [CQa CQb CQc]

where
	IQ1 - incoming question ID
	CQx - corpus question ID
"""
    Document indexing library
"""

from helper import *
from operator import itemgetter
        
class DocIndex:
    def __init__(self):
        self.documents = {}
       
    def add_document(self,tokens,doc_id):
        if doc_id not in self.documents.keys():
            self.documents[doc_id] = {}
        
        for token in tokens:
            token_stem = stem(token)
            if token_stem in self.documents[doc_id].get(token_stem, []):
                self.documents[doc_id][token_stem][0] += 1
            else:
                self.documents[doc_id][token_stem] = [1, token]
    
    def num_documents(self):
        return len(self.documents.keys())
    
    def get_document(self,doc_id):
        return self.documents.get(doc_id, None)
        
    def tf(self,token,doc_id):
        return self.documents[doc_id].get(stem(token), [0])[0]

    def max_tf(self,doc_id):
        return max([self.documents[doc_id][k][0] for k in self.documents[doc_id]])
        
    def df(self,token):
        return sum([1 if self.documents[doc_id].has_key(stem(token)) else 0 for doc_id in self.documents.keys()])
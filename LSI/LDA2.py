import nltk
from gensim import corpora, models, similarities
import re
import os
import sys
import codecs
import numpy 
import string

# sys.setdefaultencoding() does not exist, here!
#import sys
#reload(sys)  # Reload does the trick!
#sys.setdefaultencoding('UNICODE')
#read question corpus------------------------------------------
qcorpus = []
cinput = open('ICHI-Challenge-Sample-Question-Corpus.txt','r');
no = 0
topic = ''
for line in cinput:
	line = line.strip()
	if line!='':
		if '[' in line:
			no = int((line.split(']',1)[0]).split('[',1)[1])
			topic = line.split(']',1)[1].strip()
		else:
			question = line.strip()
			qcorpus.append([no,topic,question])
#read test corpus----------------------------------------------
tcorpus = []
tinput = open('ICHI-Challenge-Sample-Test-Questions.txt','r');
no = 0
topic = ''
for line in tinput:
	line = line.strip()
	if line!='':
		if '[' in line:
			no = int((line.split(']',1)[0]).split('[',1)[1])
			topic = line.split(']',1)[1].strip()
		else:
			question = line.strip()
			tcorpus.append([no,topic,question])

#print qcorpus[0]
documents = [q[2] for q in qcorpus]
#print 'I am gonna print'
#print documents[0]
#print 'I am done'
###################################################
#lowercasing, stopword removal (stemming also possible to do, also removing words which appears only once)
#stoplist = set('for a of the and to in'.split())
stoplist = nltk.corpus.stopwords.words('english')
texts = [[word for word in document.lower().split() if word not in stoplist] for document in documents]

#print texts[0]
# make a dictionary of all the words that appear in the texts, and save it.
# this assigns all wors a unique ID
dictionary = corpora.Dictionary(texts)
# this prints all the terms with their unique number ID that has been assigned to them : print(dictionary.token2id)
# saving dictionary to a file, and loading it back in:
dictionary.save('deerwester.dict')
dictionary = corpora.Dictionary.load('deerwester.dict')

# this dictionary object can then represent new documents in the space of these words
# (documents represented through terms)
new_doc = "Human computer interaction"
new_vec = dictionary.doc2bow(new_doc.lower().split())
#print(new_vec)
# [(0, 1), (1, 1)]
# This says: word 0 (computer) occurs once, word 1 (human) occurs once. no other words appeared.

# convert all our documents to this format
corpus = [dictionary.doc2bow(text) for text in texts]

# store the data in Matrix Market format:
# storing only the nonzero entries in a line of format
# rowno colno entry
corpora.MmCorpus.serialize('corpus.mm', corpus)
# and load it back in
corpus = corpora.MmCorpus('corpus.mm')

# tf/idf conversion. This does not convert the corpus,
# it just generates an object that can do the conversion
tfidf = models.TfidfModel(corpus)
'''print 'Printing the corpus[0]:'
print corpus[0]
print 'Printing the tfidf[corpus[0]]:'
print tfidf[corpus[0]]'''
# this does not create all the numbers,
# rather it creates an object that can create the numbers on the fly
corpus_tfidf = tfidf[corpus]

# we can iterate over texts in the corpus, and individual weights in each text
#for docid, doc in enumerate(corpus_tfidf):
#    for termid, val in doc: print docid, termid, val

# storing the same info in a numpy array
'''s = numpy.zeros((len(corpus_tfidf),len(dictionary)))
for docid, doc in enumerate(corpus_tfidf):
    for termid, val in doc:
        s[docid, termid] = val'''

# creating an object that does LSI conversions: SVD
#lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=5)
lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=5)
# and create a wrapper around the tfidf wrapper that creates these numbers on the fly
#corpus_lsi = lsi[corpus_tfidf]
corpus_lsi = lsi[corpus]
#for doc in corpus_lsi: print doc

'''
def svd_transform(space, keepnumdimensions):
    umatrix, sigmavector, vmatrix = numpy.linalg.svd(space)

    # remove the last few dimensions of u and sigma
    utrunc = umatrix[:, :keepnumdimensions]
    sigmatrunc = sigmavector[ :keepnumdimensions]

    # new space: U %matrixproduct% Sigma_as_diagonal_matrix    
    return numpy.dot(utrunc, numpy.diag(sigmatrunc))

# this does not give us the same thing as the LSI transform,
# the first dimension is inverted
svd_transform(s)'''


####
# inspecting the topics in an LSI object
#lsi.print_topics()
#for topicindex, topic in enumerate(lsi.print_topics()):
#    print "topic", topicindex, topic
#exit()

##Write the results of LSI model to the file

questions = [t[2] for t in tcorpus]
test_texts = [[word for word in document.lower().split() if word not in stoplist] for document in questions]
test_corpus = [dictionary.doc2bow(text) for text in test_texts]
test_corpus_lsi = lsi[test_corpus]

index = similarities.MatrixSimilarity(lsi[corpus]) # transform corpus to LSI space and index it
index.save('deerwester.index')
index = similarities.MatrixSimilarity.load('deerwester.index')
#Now use cosine 
for i,v in enumerate(test_corpus_lsi):
	sims = index[v]
	print questions[i]
	print '\n'
	print '---------------------'
	sims = sorted(enumerate(sims), key=lambda item: -item[1])
	for j in range(0,3):
		print documents[sims[j][0]]
		print '\n'
	print '---------------------'
	
'''
############
# LDA topic models
import nltk
import string

##
# preprocessing a collection of texts,
# that is, a list of word lists
def preprocess(texts):
    stopword_filename = "/Users/katrinerk/Teaching/2015/compsemantics/demos/stopwords-augmented.txt"
    f = open(stopword_filename)
    stopwords = set(f.read().split())
    f.close()
    stopwords.add("--")
    stopwords.add("``")
    stopwords.add("''")
    for punct in string.punctuation:
        stopwords.add(punct)

    return [ [w.lower() for w in text if w.lower() not in stopwords] for text in texts]

##
# making and displaying an LDA topic model
# input: a collection of texts,
# that is, a list of word lists
def make_and_show_lda_model(texts, numtopics, textlabels, show_docs = True, show_sims = True):
    # map terms to IDs
    dictionary = gensim.corpora.Dictionary(texts)
    # and represent the corpus in sparse matrix format, bag-of-words
    corpus = [dictionary.doc2bow(text) for text in texts]
    # now we make an LDA object.
    # in case we have a larger text collection (such as the Brown corpus),
    # make sure to set "passes" to a reasonably high number in order not to have all topics
    # come out equal. 20 seems to work.
    lda_obj = gensim.models.ldamodel.LdaModel(corpus, id2word=dictionary, num_topics=numtopics, passes = 20)


    # how do our texts look: how important is each topic there?
    if show_docs:
        lda_corpus = lda_obj[corpus]
        for docindex, doc in enumerate(lda_corpus):
            print docindex,
            for word in texts[docindex][:20]: print word,
            print 
            for topic, weight in doc: print str(topic) + ":" + str(round(weight, 2)),
            print
            print

    # similarities between texts?
    if show_sims:
        sim_obj = gensim.similarities.MatrixSimilarity(lda_corpus)

        for docindex, doc in enumerate(lda_corpus):
            # determine degree of similarity of this document to all other documents
            sims = sim_obj[ doc ]
            # pair similarities with document labels, sort by similarity,
            # highest first
            sims_and_labels = sorted(zip(sims, textlabels), reverse=True)
        
            print "Similarities for", textlabels[ docindex]
            # print nonzero similarities
            for sim, textlabel in sims_and_labels:
                if textlabel != textlabels[docindex] and sim > 0.0:
                    print "\t", textlabel, sim
            print

    # a look at the topics
    for index, t in enumerate(lda_obj.print_topics(numtopics, 20)):
        print index, t
        print

    

###########
# making an LDA topic model from the inaugural addresses
texts = preprocess([list(nltk.corpus.inaugural.words(fileid)) for fileid in nltk.corpus.inaugural.fileids()])
# skip address 54 because of some non-ascii character
fileids = nltk.corpus.inaugural.fileids()
# try doing the following step twice: You will get different models each time. 
make_and_show_lda_model(texts[:54] + texts[55:], 10, fileids[:54] + fileids[55:])

#############
# making an LDA topic model from the whole Brown corpus
# categories?
print "Brown categories:" ,nltk.corpus.brown.categories()
# let's build a corpus out of them
texts = preprocess([list(nltk.corpus.brown.words(fileid)) for fileid in nltk.corpus.brown.fileids()])
make_and_show_lda_model(texts, 50, nltk.corpus.brown.fileids(), show_docs = False, show_sims = False)

'''
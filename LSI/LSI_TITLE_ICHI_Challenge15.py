import nltk
from gensim import corpora, models, similarities
import re
import os
import sys
import codecs
import numpy 
import string

#from nltk.stem.snowball import SnowballStemmer
from nltk.stem.snowball import PorterStemmer
stemmer = PorterStemmer() #Portret #Lancaster

# sys.setdefaultencoding() does not exist, here!
import sys
reload(sys)  # Reload does the trick!
sys.setdefaultencoding("UTF-8")
#read question corpus------------------------------------------
qcorpus = []
cinput = open('ICHI-Challenge-Sample-Question-Corpus.txt','r');
no = 0
topic = ''
question = ""
for line in cinput:
	line = line.strip()
	if line!='':
		if '[' in line:
			no = int((line.split(']',1)[0]).split('[',1)[1])
			topic = line.split(']',1)[1].strip()
		else:
			question+=str(line.strip())
	else:
		qcorpus.append([no,topic,question])
		question = ""
#read test corpus----------------------------------------------
tcorpus = []
tinput = open('ICHI-Challenge-Sample-Test-Questions.txt','r');
no = 0
topic = ''
question = ""
for line in tinput:
	line = line.strip()
	if line!='':
		if '[' in line:
			no = int((line.split(']',1)[0]).split('[',1)[1])
			topic = line.split(']',1)[1].strip()
		else:
			question+=str(line.strip())		
	else:
		tcorpus.append([no,topic,question])
		question = ""

documents = [q[2] for q in qcorpus]
titles = [q[1] for q in qcorpus]
###################################################
#lowercasing, stopword removal (stemming also possible to do, also removing words which appears only once)
#stoplist = set('for a of the and to in'.split())
#stoplist = nltk.corpus.stopwords.words('english')
stoplist= ['\'s','a','about','above','across','after','against','ah','alas','all','along','alongside','also','although','amid','amidst','among','an','and','anyway','are','around','as','aside','at','athwart','atop','barring','be','because','before','behind','below','beneath','beside','besides','between','beyond','both','but','by','can','case','circa','concerning','could','despite','down','during','except','excluding','farewell','finally','following ','for','from','goodbye','he','hello','hence','her','here','hers','hi','him','hmm','however','I','if','in','inside','into','is','it','its','lastly','like','mid','minus','near','nevertheless','next','no','nor','not','notwithstanding','of','other','off','oh','on','onto','opposite','or','ouch','out','outside','over','pace','past','per','person','plus','qua','regarding ','round ','\'s','save','she','since','since','so','still','than','that','the','their','theirs','them','then','there','these','they','this','those','though','through','throughout','thus','till','times','to','toward','towards','under','underneath','unless','unlike','until','up','upon','versus','via','was','well','were','what','when','whenever','where','wherever','which','while','who','whole','whom','with','within','without','worth','would','wow','yes','yet','-lrb-','-rrb-']

def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
	#print filtered_tokens
    stems = [stemmer.stem(str(t)) for t in filtered_tokens]
    return filtered_tokens
	
def strip_proppers(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent) if word.islower()]
    return "".join([" "+i if not i.startswith("'") and i not in string.punctuation else i for i in tokens]).strip()
	#return "".join(tokens).strip()

preprocess = [strip_proppers(doc) for doc in documents]
tpreprocess = [strip_proppers(title) for title in titles]
#tokenize
#print tpreprocess
tokenized_text = [tokenize_and_stem(text) for text in preprocess]
tokenized_title = [tokenize_and_stem(title) for title in tpreprocess]

#remove stop words
#print tokenized_title
texts = [[str(word) for word in text if word not in stoplist] for text in tokenized_text]
ttexts = [[str(word) for word in title if word not in stoplist] for title in tokenized_title]
#texts = [[word for word in document.lower().split() if word not in stoplist] for document in documents]

# make a dictionary of all the words that appear in the texts, and save it.
# this assigns all words a unique ID
#alltext = texts
#alltext.extend(ttexts)
dictionary = corpora.Dictionary(texts)
print dictionary
# this prints all the terms with their unique number ID that has been assigned to them : print(dictionary.token2id)
# saving dictionary to a file, and loading it back in:
dictionary.save('deerwester.dict')
dictionary = corpora.Dictionary.load('deerwester.dict')
for k,v in dictionary.iteritems():
	print v
# convert all our documents to this format
corpus = [dictionary.doc2bow(text) for text in texts]
#print ttexts
title_corpus = [dictionary.doc2bow(title) for title in ttexts]
print ttexts
# store the data in Matrix Market format:
# storing only the nonzero entries in a line of format
# rowno colno entry
corpora.MmCorpus.serialize('corpus.mm', corpus)
corpora.MmCorpus.serialize('title_corpus.mm', title_corpus)
# and load it back in
#corpus = corpora.MmCorpus('corpus.mm')
#title_corpus = corpora.MmCorpus('title_corpus.mm')

# tf/idf conversion. This does not convert the corpus,
# it just generates an object that can do the conversion
tfidf = models.TfidfModel(corpus)
title_tfidf = models.TfidfModel(title_corpus)
# this does not create all the numbers,
# rather it creates an object that can create the numbers on the fly
corpus_tfidf = tfidf[corpus]
corpus_title_tfidf = title_tfidf[title_corpus]


# creating an object that does LSI conversions
lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=50)
title_lsi = models.LsiModel(title_corpus, id2word=dictionary, num_topics=50)
corpus_lsi = lsi[corpus]
title_corpus_lsi = title_lsi[title_corpus]

#Write the results of LSI model to the file
questions = [t[2] for t in tcorpus]
titles_questions = [t[1] for t in tcorpus]

#test_preprocess = [strip_proppers(doc) for doc in questions]
test_preprocess = questions
#test_titles_preprocess = [strip_proppers(title) for title in titles_questions]
test_titles_preprocess = titles_questions
#tokenize
test_tokenized_text = [tokenize_and_stem(text) for text in test_preprocess]
test_tokenized_title = [tokenize_and_stem(title) for title in test_titles_preprocess]
#remove stop words
test_texts = [[str(word) for word in text if word not in stoplist] for text in test_tokenized_text]
test_titles = [[str(word) for word in title if word not in stoplist] for title in test_tokenized_title]

test_corpus = [dictionary.doc2bow(text) for text in test_texts]
test_title_corpus = [dictionary.doc2bow(title) for title in test_titles]

test_corpus_lsi = lsi[test_corpus]
test_title_corpus_lsi = title_lsi[test_title_corpus]

index = similarities.MatrixSimilarity(lsi[corpus]) # transform corpus to LSI space and index it
title_index = similarities.MatrixSimilarity(title_lsi[title_corpus]) # transform corpus to LSI space and index it

index.save('deerwester.index')
title_index.save('title.index')

index = similarities.MatrixSimilarity.load('deerwester.index')
title_index = similarities.MatrixSimilarity.load('title.index')
#Now use cosine 
output = open('output_stem_title.txt','w')
#similarities = []
import operator
#print title_corpus
for i,v in enumerate(test_corpus_lsi):
	sims = {}
	sims1 = index[v]#Check test body is similar to corpus bodyssssssssssssss
	sims2 = index[title_lsi[test_title_corpus[i]]] #Check test title similar to corpus body
	sims3 = title_index[v]#Check test body is equal to corpus title
	sims4 = title_index[title_lsi[test_title_corpus[i]]] #check test title is similar to corpus title

	dic_sims1 = dict(sorted(enumerate(sims1), key=lambda item: -item[1]))
	dic_sims2 = dict(sorted(enumerate(sims2), key=lambda item: -item[1]))
	dic_sims3 = dict(sorted(enumerate(sims3), key=lambda item: -item[1]))
	dic_sims4 = dict(sorted(enumerate(sims4), key=lambda item: -item[1]))
	for k,v in dic_sims1.iteritems():
		sims[k] = dic_sims1[k]+dic_sims4[k]
		#sims[k] = dic_sims4[k]
		sims[k] = dic_sims1[k]+dic_sims2[k]+dic_sims3[k]+dic_sims4[k]
	sorted_sims = sorted(sims.items(), key=operator.itemgetter(1),reverse=True)
	op = "["+str(tcorpus[i][0])+"]"+" "+tcorpus[i][1]+" "+str(qcorpus[sorted_sims[0][0]][0])+" "+str(qcorpus[sorted_sims[1][0]][0])+" "+str(qcorpus[sorted_sims[2][0]][0])+"\n"+tcorpus[i][2]+"\n"
	output.write(op)
	
	#Add stemming : Synonyms
	#SRI LM

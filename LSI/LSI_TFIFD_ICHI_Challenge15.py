import nltk
from gensim import corpora, models, similarities
import re
import os
import sys
import codecs
import numpy 
import string

# sys.setdefaultencoding() does not exist, here!
#import sys
#reload(sys)  # Reload does the trick!
#sys.setdefaultencoding('UNICODE')
#read question corpus------------------------------------------
qcorpus = []
cinput = open('ICHI-Challenge-Sample-Question-Corpus.txt','r');
no = 0
topic = ''
question = ""
for line in cinput:
	line = line.strip()
	if line!='':
		if '[' in line:
			no = int((line.split(']',1)[0]).split('[',1)[1])
			topic = line.split(']',1)[1].strip()
		else:
			question+=str(line.strip())
	else:
		qcorpus.append([no,topic,question])
		question = ""
#read test corpus----------------------------------------------
tcorpus = []
tinput = open('ICHI-Challenge-Sample-Test-Questions.txt','r');
no = 0
topic = ''
question = ""
for line in tinput:
	line = line.strip()
	if line!='':
		if '[' in line:
			no = int((line.split(']',1)[0]).split('[',1)[1])
			topic = line.split(']',1)[1].strip()
		else:
			question+=str(line.strip())		
	else:
		tcorpus.append([no,topic,question])
		question = ""

documents = [q[2] for q in qcorpus]

###################################################
#lowercasing, stopword removal (stemming also possible to do, also removing words which appears only once)
#stoplist = set('for a of the and to in'.split())
stoplist = nltk.corpus.stopwords.words('english')
texts = [[word for word in document.lower().split() if word not in stoplist] for document in documents]

# make a dictionary of all the words that appear in the texts, and save it.
# this assigns all wors a unique ID
dictionary = corpora.Dictionary(texts)
# this prints all the terms with their unique number ID that has been assigned to them : print(dictionary.token2id)
# saving dictionary to a file, and loading it back in:
dictionary.save('deerwester.dict')
dictionary = corpora.Dictionary.load('deerwester.dict')

# this dictionary object can then represent new documents in the space of these words
# (documents represented through terms)
new_doc = "Human computer interaction"
new_vec = dictionary.doc2bow(new_doc.lower().split())
#print(new_vec)
# [(0, 1), (1, 1)]
# This says: word 0 (computer) occurs once, word 1 (human) occurs once. no other words appeared.

# convert all our documents to this format
corpus = [dictionary.doc2bow(text) for text in texts]

# store the data in Matrix Market format:
# storing only the nonzero entries in a line of format
# rowno colno entry
corpora.MmCorpus.serialize('corpus.mm', corpus)
# and load it back in
corpus = corpora.MmCorpus('corpus.mm')

# tf/idf conversion. This does not convert the corpus,
# it just generates an object that can do the conversion
tfidf = models.TfidfModel(corpus)
# this does not create all the numbers,
# rather it creates an object that can create the numbers on the fly
corpus_tfidf = tfidf[corpus]


# creating an object that does LSI conversions
lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=20)
corpus_lsi = lsi[corpus_tfidf]

#Write the results of LSI model to the file
questions = [t[2] for t in tcorpus]
test_texts = [[word for word in document.lower().split() if word not in stoplist] for document in questions]
test_corpus = [dictionary.doc2bow(text) for text in test_texts]
tfidf_test_corpus = tfidf[test_corpus]
tfidf_test_corpus_lsi = lsi[tfidf_test_corpus]

index = similarities.MatrixSimilarity(lsi[corpus_tfidf]) # transform corpus to LSI space and index it
index.save('deerwester.index')
index = similarities.MatrixSimilarity.load('deerwester.index')
#Now use cosine 
output = open('output.txt','w')
for i,v in enumerate(tfidf_test_corpus_lsi):
	sims = index[v]
	print questions[i]
	print '\n'
	print '---------------------'
	sims = sorted(enumerate(sims), key=lambda item: -item[1])
	for j in range(0,3):
		print documents[sims[j][0]]
		print '\n'
	print '---------------------'
	op = "["+str(tcorpus[i][0])+"]"+" "+tcorpus[i][1]+" "+str(qcorpus[sims[0][0]][0])+" "+str(qcorpus[sims[1][0]][0])+" "+str(qcorpus[sims[2][0]][0])+"\n"+tcorpus[i][2]+"\n"
	output.write(op)
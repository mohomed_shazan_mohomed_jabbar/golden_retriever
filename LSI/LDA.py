import nltk
from gensim import corpora, models, similarities
import re
import os
import sys
import codecs
import numpy as np
import string

reload(sys)
sys.setdefaultencoding('Cp1252')
#read question corpus
qcorpus = []
cinput = open('ICHI-Challenge-Sample-Question-Corpus.txt','r');
no = 0
topic = ''
for line in cinput:
	line = line.strip()
	if line!='':
		if line[0]=='[':
			no = int(line[1:len(line)].split(']',1)[0])
			topic = line[1:len(line)].split(']',1)[1].strip()
		else:
			question = line.strip()
			qcorpus.append([no,topic,question])
#print qcorpus

#read test corpus
tcorpus = []
tinput = open('ICHI-Challenge-Sample-Test-Questions.txt','r');
no = 0
topic = ''
for line in tinput:
	line = line.strip()
	if line!='':
		if line[0]=='[':
			no = int(line[1:len(line)].split(']',1)[0])
			topic = line[1:len(line)].split(']',1)[1].strip()
		else:
			question = line.strip()
			tcorpus.append([no,topic,question])
#print tcorpus

# load nltk's English stopwords as variable called 'stopwords'
stopwords = nltk.corpus.stopwords.words('english')
#print stopwords[:10]

# load nltk's SnowballStemmer as variabled 'stemmer'
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")

def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

questions = [q[2] for q in qcorpus]
#print len(questions)
#print questions[0]
#print questions[1]

#not super pythonic, no, not at all.
#use extend so it's a big flat list of vocab
totalvocab_stemmed = []
totalvocab_tokenized = []
for i in questions:
	allwords_stemmed = tokenize_and_stem(i) #for each item in 'synopses', tokenize/stem
	totalvocab_stemmed.extend(allwords_stemmed) #extend the 'totalvocab_stemmed' list
	allwords_tokenized = tokenize_only(i)
	totalvocab_tokenized.extend(allwords_tokenized)

def strip_proppers(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent) if word.islower()]
    return "".join([" "+i if not i.startswith("'") and i not in string.punctuation else i for i in tokens]).strip()
	
#remove proper names
preprocess = [strip_proppers(doc) for doc in questions]

#tokenize
tokenized_text = [tokenize_and_stem(text) for text in preprocess]

#remove stop words
texts = [[word for word in text if word not in stopwords] for text in tokenized_text]


#create a Gensim dictionary from the texts
dictionary = corpora.Dictionary(texts)

#remove extremes (similar to the min/max df step used when creating the tf-idf matrix)
dictionary.filter_extremes(no_below=1, no_above=0.8)

#convert the dictionary to a bag of words corpus for reference
corpus = [dictionary.doc2bow(text) for text in texts]

lda = models.LdaModel(corpus, num_topics=10,id2word=dictionary,update_every=5,chunksize=10000,passes=100)

print lda.show_topics()

test = tcorpus[0][2]
#pp_test = strip_proppers(test)
#tk_test = tokenize_and_stem(pp_test)
#test = [word for word in tk_test if word not in stopwords]

vec_bow = dictionary.doc2bow(test.lower().split())
vec_lda = lda[vec_bow]

index = similarities.MatrixSimilarity(lda[corpus]) # transform corpus to LSI space and index it

sims = index[vec_lda] # perform a similarity query against the corpus
sims = sorted(enumerate(sims), key=lambda item: -item[1])
print(sims)
print '\n\n'
for i in sims[:3]:
	print qcorpus[i[0]]
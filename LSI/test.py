import nltk
from gensim import corpora, models, similarities
import re
import os
import sys
import codecs
import numpy 
import string

documents = ["Human machine interface for lab abc computer applications","A survey of user opinion of computer system response time","The EPS user interface management system","System and human system engineering testing of EPS","Relation of user perceived response time to error measurement","The generation of random binary unordered trees","The intersection graph of paths in trees","Graph minors IV Widths of trees and well quasi ordering","Graph minors A survey"]
stoplist = set('for a of the and to in'.split())
texts = [[str(word).encode("UTF-8") for word in document.lower().split() if word not in stoplist] for document in documents]

# remove words that appear only once
from collections import defaultdict
frequency = defaultdict(int)
for text in texts:
	for token in text:
		frequency[token] += 1
		
texts = [[token for token in text if frequency[token] > 1] for text in texts]
from pprint import pprint   # pretty-printer
pprint(texts)
dictionary = corpora.Dictionary(texts)
pprint(dictionary.token2id)
exit()
# this prints all the terms with their unique number ID that has been assigned to them : print(dictionary.token2id)
# saving dictionary to a file, and loading it back in:
dictionary.save('deerwester.dict')
dictionary = corpora.Dictionary.load('deerwester.dict')
for k,v in dictionary.iteritems():
	print v
# convert all our documents to this format
corpus = [dictionary.doc2bow(text) for text in texts]
#print ttexts
title_corpus = [dictionary.doc2bow(title) for title in ttexts]
print ttexts
# store the data in Matrix Market format:
# storing only the nonzero entries in a line of format
# rowno colno entry
corpora.MmCorpus.serialize('corpus.mm', corpus)
corpora.MmCorpus.serialize('title_corpus.mm', title_corpus)
# and load it back in
#corpus = corpora.MmCorpus('corpus.mm')
#title_corpus = corpora.MmCorpus('title_corpus.mm')

# tf/idf conversion. This does not convert the corpus,
# it just generates an object that can do the conversion
tfidf = models.TfidfModel(corpus)
title_tfidf = models.TfidfModel(title_corpus)
# this does not create all the numbers,
# rather it creates an object that can create the numbers on the fly
corpus_tfidf = tfidf[corpus]
corpus_title_tfidf = title_tfidf[title_corpus]


# creating an object that does LSI conversions
lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=50)
title_lsi = models.LsiModel(title_corpus, id2word=dictionary, num_topics=50)
corpus_lsi = lsi[corpus]
title_corpus_lsi = title_lsi[title_corpus]

#Write the results of LSI model to the file
questions = [t[2] for t in tcorpus]
titles_questions = [t[1] for t in tcorpus]

#test_preprocess = [strip_proppers(doc) for doc in questions]
test_preprocess = questions
#test_titles_preprocess = [strip_proppers(title) for title in titles_questions]
test_titles_preprocess = titles_questions
#tokenize
test_tokenized_text = [tokenize_and_stem(text) for text in test_preprocess]
test_tokenized_title = [tokenize_and_stem(title) for title in test_titles_preprocess]
#remove stop words
test_texts = [[str(word) for word in text if word not in stoplist] for text in test_tokenized_text]
test_titles = [[str(word) for word in title if word not in stoplist] for title in test_tokenized_title]

test_corpus = [dictionary.doc2bow(text) for text in test_texts]
test_title_corpus = [dictionary.doc2bow(title) for title in test_titles]

test_corpus_lsi = lsi[test_corpus]
test_title_corpus_lsi = title_lsi[test_title_corpus]

index = similarities.MatrixSimilarity(lsi[corpus]) # transform corpus to LSI space and index it
title_index = similarities.MatrixSimilarity(title_lsi[title_corpus]) # transform corpus to LSI space and index it

index.save('deerwester.index')
title_index.save('title.index')

index = similarities.MatrixSimilarity.load('deerwester.index')
title_index = similarities.MatrixSimilarity.load('title.index')
#Now use cosine 
output = open('output_stem_title.txt','w')
#similarities = []
import operator
#print title_corpus
for i,v in enumerate(test_corpus_lsi):
	sims = {}
	sims1 = index[v]#Check test body is similar to corpus bodyssssssssssssss
	sims2 = index[title_lsi[test_title_corpus[i]]] #Check test title similar to corpus body
	sims3 = title_index[v]#Check test body is equal to corpus title
	sims4 = title_index[title_lsi[test_title_corpus[i]]] #check test title is similar to corpus title

	dic_sims1 = dict(sorted(enumerate(sims1), key=lambda item: -item[1]))
	dic_sims2 = dict(sorted(enumerate(sims2), key=lambda item: -item[1]))
	dic_sims3 = dict(sorted(enumerate(sims3), key=lambda item: -item[1]))
	dic_sims4 = dict(sorted(enumerate(sims4), key=lambda item: -item[1]))
	for k,v in dic_sims1.iteritems():
		sims[k] = dic_sims1[k]+dic_sims4[k]
		#sims[k] = dic_sims4[k]
		sims[k] = dic_sims1[k]+dic_sims2[k]+dic_sims3[k]+dic_sims4[k]
	sorted_sims = sorted(sims.items(), key=operator.itemgetter(1),reverse=True)
	op = "["+str(tcorpus[i][0])+"]"+" "+tcorpus[i][1]+" "+str(qcorpus[sorted_sims[0][0]][0])+" "+str(qcorpus[sorted_sims[1][0]][0])+" "+str(qcorpus[sorted_sims[2][0]][0])+"\n"+tcorpus[i][2]+"\n"
	output.write(op)
	
	#Add stemming : Synonyms
	#SRI LM

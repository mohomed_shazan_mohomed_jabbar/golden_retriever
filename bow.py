'''
    Bag of words extractor
'''

from sys import argv
from helper import *

def main(quest_file):
    stop_words = [stem(w) for w in load_file('glasgow')]
    med_words = load_file('med_words')
    med_words = [stem(w.decode('latin1').encode('ascii', 'xmlcharrefreplace')) for w in med_words]
    syndict = load_thesaurus('mobythes.aur')
    abbr = load_abbreviations('abbreviations')
    
    corpus_questions = load_questions(quest_file)
    
    for cquestion in corpus_questions:
        id = cquestion[0]
        print id
        
        title = cquestion[1]
        title_tokens = tokenize(title,stop_words)
        old_tt = list(title_tokens)
        print deduplicate(title_tokens)
        
        append_abbr(title_tokens,abbr)
        print deduplicate(list(set(title_tokens).difference(set(old_tt))))
        
        title_syns = deduplicate(flatten([synonyms(t,syndict) for t in title_tokens]))
        print [s for s in title_syns if stem(s) in med_words or in_title(s,id,corpus_questions,stop_words)]
        
        body = cquestion[2]
        body_tokens = tokenize(body,stop_words)
        print deduplicate(body_tokens)
        old_bt = list(body_tokens)
        
        append_abbr(body_tokens,abbr)
        print deduplicate(list(set(body_tokens).difference(set(old_bt))))
        
        body_syns = deduplicate(flatten([synonyms(t,syndict) for t in body_tokens]))
        print [s for s in body_syns if stem(s) in med_words or in_title(s,id,corpus_questions,stop_words)]
        print
    
if __name__ == '__main__':
    if len(argv) == 2:
        main(argv[1])
    else:
        print('Usage: python bow.py questions')
        print(' python - Version 2.7+')
        print(' questions - Questions file')
'''
    Helper functions
'''

from __future__ import division
from re import split,compile
from nltk.stem.lancaster import LancasterStemmer

def synonyms(word,syndict):
    if word in syndict.keys():
        syns = syndict[word]
        return flatten([s.lower().split() for s in syns])
    else:
        return [] 

def append_abbr(tokens,abbr):
    for token in tokens:
        if token in abbr.keys():
            abbr_tokens = abbr[token]
            for t in abbr_tokens:
                for tk in t.split():
                    tokens.append(tk)
    return tokens
    
def in_title(word,qid,corpus,stop_words):
    for cquestion in corpus:
        cid = cquestion[0]
        ctitle = cquestion[1]
        if cid == qid and stem(word) in [stem(t) for t in tokenize(ctitle,stop_words)]:
            return True
    return False

def load_abbreviations(filename):
    f = open(filename)
    abbr = {}
    for line in f.readlines():
        parts = line.strip().lower().split(',')
        abbr[parts[0]] = parts[1:]
    return abbr

def load_thesaurus(filename):
    thes = {}
    with open(filename) as fp:
        f = fp.read()     
        line = ''
        for char in f:
            if char != '\r':
                line += char
            else:
                line = line.split(',')
                for word in line[1:]:
                    thes.setdefault(line[0], []).append(word)
                line = ''
    return thes

def load_results(results):
    ordered = {}
    for result in results:
        result = result.strip()
        id = int(result[:result.find(' [')])
        ordered[id] = [int(r) for r in result[result.find(' [')+2:-1].split()]
    return ordered

def recall(my_results, expert_results):
    recalls = []
    for my_id in my_results.keys():
        expert_answer = expert_results[my_id]
        my_answer = my_results[my_id]
        common_answers = len(list(set(expert_answer) & set(my_answer)))
        recall_score = common_answers / len(expert_answer)
        recalls.append([my_id,round(recall_score,2)])
    return recalls

def load_questions(file):
        rows = load_file(file)
        id = None
        title = None
        body = None
        questions = []
        read_title = False
        read_body = False
        for row in rows:
            if row.find(']') is not -1:
                read_title = True

            if read_title and read_body:
                questions.append([id,title,body])

            if read_body:
                body = row.strip().lower()   
            
            if read_title:
                delim = row.find(']')
                id = row[1:delim]
                title = row[delim+1:].strip().lower()
                read_title = False
                read_body = True
        
        questions.append([id,title,body])
        return questions

def load_file(file):
    f = open(file)
    valid_lines = []
    for line in f.readlines():
        line = line.strip().lower()
        if line is not '':
            valid_lines.append(line)
    return valid_lines

def flatten(list_of_lists):
    return [val for sublist in list_of_lists for val in sublist]

def deduplicate(lst):
    seen = set()
    seen_add = seen.add
    return [x for x in lst if not (x in seen or seen_add(x))]

def tokenize(sentence,stop_words):
    tokens = compile("[^a-zA-Z0-9']").split(sentence)
    ret_tokens = []
    for token in tokens:
        if filter(token,stop_words):
            continue
        else:
            ret_tokens.append(token)
    return ret_tokens

def filter(w, stop_words):
    return w is None or w.strip() == '' or w.strip().isdigit() or stem(w.strip().lower()) in stop_words

def stem(word):
    return LancasterStemmer().stem(word)

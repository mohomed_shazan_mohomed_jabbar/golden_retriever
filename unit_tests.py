from helper import *
from docindex import DocIndex
import unittest

class TestGoldenRetriever(unittest.TestCase):
    def test_helper(self):
        expert = load_results(load_file('Evaluation/Kim_Expert_Annotations.txt'))
        self.assertEqual(expert[11], [137, 165, 193])
        self.assertEqual(expert[18], [156, 196, 142])
        my = load_results(load_file('results.txt'))
        self.assertEqual(my[11], [165, 128, 193])
        self.assertEqual(my[15], [162, 159, 122])
        recalls = recall(my,expert)
        self.assertEqual(recalls[0], [11, 0.67])
        self.assertEqual(recalls[1], [12, 0.67])

        glasgow_stop_words = load_file('glasgow')
        self.assertIn('69',glasgow_stop_words)
        self.assertEqual(10,len(load_questions('Datasets/ICHI-Challenge-Sample-Test-Questions.txt')))
        self.assertEqual(95,len(load_questions('Datasets/ICHI-Challenge-Sample-Question-Corpus.txt')))
        self.assertEqual(flatten([[1,2],[3],[4,5,6,7]]), [1,2,3,4,5,6,7])
        self.assertEqual(deduplicate([1,2,4,2,2,3,2,3,4,7,8]), [1,2,4,3,7,8])
        self.assertEqual(tokenize('hello,world! :)))',glasgow_stop_words), ['hello','world'])
        self.assertEqual(tokenize("i'm,going home now!bye?",glasgow_stop_words), ["i'm",'going','home','bye'])
        self.assertEqual(filter('619',glasgow_stop_words),True)
        self.assertEqual(filter('hello',glasgow_stop_words),False)
        self.assertEqual(filter('now',glasgow_stop_words),True)
        self.assertEqual(filter(None,glasgow_stop_words),True)
        self.assertIn('now',glasgow_stop_words)
        self.assertIn('an',glasgow_stop_words)
        self.assertEqual(stem('private'),'priv')
        self.assertEqual(stem('privacy'),'priv')
        
        abbr = load_abbreviations('abbreviations')
        self.assertIn('ed',abbr.keys())
        self.assertIn('a1c',abbr.keys())
        tokens = ['ed','education','edward']
        append_abbr(tokens,abbr)
        self.assertIn('erectile',tokens)
        self.assertIn('emergency',tokens)
        self.assertIn('department',tokens)
        
    def test_indexer(self):
        di = DocIndex()
        self.assertEqual(di.num_documents(), 0)
        stopwords = load_file('glasgow')
        
        di.add_document(tokenize('hello sugar!',stopwords), 1)
        self.assertEqual(di.get_document(1), {stem('hello'):[1,'hello'],stem('sugar'):[1,'sugar']})
        self.assertEqual(di.get_document(2), None)
        self.assertEqual(di.tf('hello',1), 1)
        self.assertEqual(di.tf('sugar',1), 1)
        self.assertEqual(di.tf('bye',1), 0)
        self.assertEqual(di.max_tf(1),1)
        self.assertEqual(di.df('hello'), 1)
        self.assertEqual(di.df('sugar'), 1)
        self.assertEqual(di.df('bye'), 0)
        self.assertEqual(di.num_documents(), 1)
        
        di.add_document(tokenize('hello, hello diabetes :)',stopwords), 2)
        self.assertEqual(di.get_document(2), {stem('hello'):[2,'hello'],stem('diabetes'):[1,'diabetes']})
        self.assertEqual(di.tf('hello',2), 2)
        self.assertEqual(di.tf('sugar',2), 0)
        self.assertEqual(di.tf('diabetes',2), 1)
        self.assertEqual(di.tf('diabetic',2), 1)
        self.assertEqual(di.max_tf(2),2)
        self.assertEqual(di.df('hello'), 2)
        self.assertEqual(di.df('diabetes'), 1)
        self.assertEqual(di.num_documents(), 2)
        
        di.add_document(tokenize('impotence :)',stopwords), 3)
        self.assertEqual(di.get_document(3), {stem('impotence'):[1,'impotence']})
        self.assertEqual(di.num_documents(), 3)
        
        di.add_document(tokenize('mechanical energy',stopwords), 4)
        di.add_document(tokenize('kinetic energy',stopwords), 4)
        self.assertEqual(di.get_document(4), {stem('mechanical'):[1,'mechanical'],stem('kinetic'):[1,'kinetic'],stem('energy'):[2,'energy']})
        self.assertEqual(di.num_documents(), 4)
        self.assertEqual(di.df('energy'), 1)
        di.add_document(tokenize('hello',stopwords), 4)
        self.assertEqual(di.df('hello'), 3)
    
if __name__ == '__main__':
    unittest.main()